#!/usr/bin/env python3
# import the necessary packages

#TUTORIAL : 

#https://www.pyimagesearch.com/2020/05/04/covid-19-face-mask-detector-with-opencv-keras-tensorflow-and-deep-learning/

import tensorflow as tf
from tensorflow import keras


from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.layers import AveragePooling2D
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.utils import to_categorical
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os

ap = argparse.ArgumentParser()

ap.add_argument('-d', '--dataset', required=True, help='Path to input dataset')
ap.add_argument('-p', '--plot', type=str, default='plot.png', help='Path to output loss/accuracy plot')
ap.add_argument('-m', '--model', type=str, default='mask_detector.model', help='path to output face mask detector model')
args = vars(ap.parse_args())

INIT_LR = 1e-4
EPOCHS = 20
BS = 32

print("[INFO] loading images...")
imagePaths = list(paths.list_images(args['dataset'])) # paths.list_images is from  imutils
#imagepaths contient list vers tous les fichiers dans dataset dossier
#['./dataset/without_mask/augmented_image_141.jpg', './dataset/without_mask/augmented_image_140.jpg', ......']

data = []
labels = []

for imagePath in imagePaths:
	break
	#Récup le nom de la label class "without_mask" a partir du nom de dossier
	label = imagePath.split(os.path.sep)[-2] #De base -2, . (=0) dataset (=1) without_mask (=2)

	image = load_img(imagePath, target_size=(224,224))
	image = img_to_array(image)
	image = preprocess_input(image)

	data.append(image)
	labels.append(label)

data = np.array(data, dtype='float32')
labels = np.array(labels)


lb = LabelBinarizer()
labels =lb.fit_transform(labels)
labels = to_categorical(labels) #créer catégorie soit 0 soit 1, pour chaque label line
#=> array([[1., 0.],..])



'''This stratify parameter makes a split 
so that the proportion of values in the sample produced will be 
the same as the proportion of values provided to parameter stratify.
For example, 
if variable y is a binary categorical variable with values 0 and 1 and there are 25% of zeros and 75% of ones, stratify=labels will make sure that your random split has 25% of 0's and 75% of 1's.
'''
trainX, testX, trainY, testY = train_test_split(data, labels, test_size=0.20, stratify=labels, random_state=42)

aug = ImageDataGenerator(
	rotation_range=20,
	zoom_range=0.15,
	width_shift_range=0.2,
	height_shift_range=0.2,
	shear_range=0.15,
	horizontal_flip=True,
	fill_mode="nearest")

'''
https://towardsdatascience.com/step-by-step-guide-to-using-pretrained-models-in-keras-c9097b647b29

To use the pretrained weights we have to set the argument weights to imagenet. 
The default value is also set to imagenet. 
But if we want to train the model from scratch, 
we can set the weights argument to None. 
This will initialize the weights randomly in the network.

We can remove the default classifier and attach our own classifier in the pretrained model. 
To exclude the default classifier we have to set argument include_top to false.

'''
baseModel = MobileNetV2(weights="imagenet", include_top=False,
	input_tensor=Input(shape=(224,224,3)))

headModel = baseModel.output
headModel = AveragePooling2D(pool_size=(7,7))(headModel)
headModel = Flatten(name="flatten")(headModel)
headModel = Dense(128, activation="relu")(headModel)
headModel = Dropout(0.5)(headModel)
headModel = Dense(2, activation="softmax")(headModel)

model = Model(inputs=baseModel.input, outputs=headModel)

# loop over all layers in the base model and freeze them so they will
# *not* be updated during the first training process

for layer in baseModel.layers:
	keras.layers.trainable = False

print("[INFO] compiling model..")
opt = Adam(lr=INIT_LR, decay=INIT_LR/ EPOCHS) # decay or schedule dictate how the learning rates change over time, reduce
model.compile(loss='binary_crossentropy', optimizer=opt, metrics=["accuracy"])

print("[INFO] training head...")
H = model.fit(
	aug.flow(trainX, trainY, batch_size=BS),
	steps_per_epoch=len(trainX) // BS,
	validation_data=(testX, testY),
	validation_steps=len(testX) // BS,
	epochs=EPOCHS)

'''
steps_per_epoch: Integer. 
Total number of steps (batches of samples) to yield from generator before declaring one epoch finished and starting the next epoch. 
It should typically be equal to ceil(num_samples / batch_size). 
Optional for Sequence: if unspecified, will use the len(generator) as a number of steps.
'''

print("[INFO] evaluating network...")
predIdxs = model.predict(testX, batch_size=BS)

predIdxs = np.argmax(predIdxs, axis=1)

print(classification_report(testY.argmax(axis=1), predIdxs, target_names=lb.classes_))

print("[INFO] saving mask detector model...")

model.save(args["model"], save_format="h5")

N = EPOCHS
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, N), H.history["accuracy"], label="train_acc")
plt.plot(np.arange(0, N), H.history["val_accuracy"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="lower left")
plt.savefig(args["plot"])
#!/usr/bin/env python3

import cv2
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.models import load_model


#dnn > haarcascade
#https://towardsdatascience.com/face-detection-models-which-to-use-and-why-d263e82c302c
#face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
#eye_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_eye.xml') 

#face_cascade.load('haarcascade_frontalface_default.xml')

# Opening image
#img = cv2.imread("./examples/example_02.png")
#img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
#gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#plt.subplot(1, 1, 1)
#plt.imshow(img_rgb)
#plt.show()

        
modelFile = "./model/res10_300x300_ssd_iter_140000.caffemodel"
configFile = "./model/deploy.proto.txt"
net = cv2.dnn.readNetFromCaffe(configFile, modelFile)

img = cv2.imread("./examples/macron.jpg")
h, w = img.shape[:2]
blob = cv2.dnn.blobFromImage(cv2.resize(img, (300, 300)), 1.0,
(300, 300), (104.0, 117.0, 123.0))

#passing blob through the network to detect and pridiction
net.setInput(blob)
faces = net.forward()

# loop over the detections
for i in range(faces.shape[2]):
        confidence = faces[0, 0, i, 2]
        print(confidence)

        if confidence > 0.5:
            box = faces[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")
            cv2.rectangle(img, (startX, startY), (endX, endY), (0, 0, 255), 2)

            face = img[startY:endY,startX:endX]
            face = cv2.resize(face, (224,224))  # resize
            face = np.array(face).astype(np.float32)/255.0  # scaling
            face = np.expand_dims(face, axis=0)  # expand dimension
            model = load_model('model.h5')
            (mask, withoutMask) = model.predict(face)[0]
            #image = cv2.resize(image, dimensions, cv2.INTER_AREA)
            # determine the class label and color we'll use to draw
            # the bounding box and text
            if mask > withoutMask:
                label = "Mask"
            else:
                label = "No Mask"
            
            if label == "Mask":
                color = (0, 255, 0) 
            else:
                color = (0, 0, 255)
                # include the probability in the label
            label = "{}: {:.2f}%".format(label, max(mask, withoutMask) * 100)
            # display the label and bounding box rectangle on the output
            # frame
            cv2.putText(img, label, (startX,startY - 10),cv2.FONT_HERSHEY_SIMPLEX, 0.45, color, 2)
            cv2.rectangle(img, (startX, startY), (endX, endY), color, 2)

#face = roi_color    
#face = img_to_array(face)
#face = preprocess_input(face)
#face = np.expand_dims(face, axis=0)
#print(face.shape)
#face = cv2.resize(face, (28, 28))
# pass the face through the model to determine if the face
# has a mask or not



cv2.imshow('img',img)
k = cv2.waitKey(0)
if k == 27:         # wait for ESC key to exit
    cv2.destroyAllWindows()
elif k == ord('s'): # wait for 's' key to save and exit
    cv2.imwrite('new_img.png',img)
    cv2.destroyAllWindows()